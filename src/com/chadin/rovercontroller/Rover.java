package com.chadin.rovercontroller;

public class Rover {
	
	public static final char DIRECTION_EAST = 'E';
	public static final char DIRECTION_WEST = 'W';
	public static final char DIRECTION_NORTH = 'N';
	public static final char DIRECTION_SOUTH = 'S';
	public static final String ERROR_MOVE_COLLISION = "Unable to move there, there is something in that location";
	private static final String ERROR_MOVE_INVALID_NUM_BLOCKS = "Only 1 and -1 are allowed as numBlocks";
	
	private Landscape landscape;
	private int x;
	private int y;
	private char direction;
	
	public Rover(Landscape landscape, int x, int y, char direction) {
		this.landscape = landscape;
		this.x = x;
		this.y = y;
		this.setDirection(direction);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public char getDirection() {
		return direction;
	}

	public void setDirection(char direction) {
		char[] validDirections = {Rover.DIRECTION_EAST,
				Rover.DIRECTION_NORTH,
				Rover.DIRECTION_SOUTH,
				Rover.DIRECTION_WEST
		};
		for (int i = 0; i < validDirections.length; i++) {
			if (direction == validDirections[i]) {
				//Valid
				this.direction = direction;
				return;
			}
		}
		throw new IllegalArgumentException("Invalid direction to set");
	}
	
	public String getHumanFriendlyDirection() {
		switch (this.getDirection()) {
		case Rover.DIRECTION_EAST:
			return "East";
		case Rover.DIRECTION_NORTH:
			return "North";
		case Rover.DIRECTION_SOUTH:
			return "South";
		case Rover.DIRECTION_WEST:
			return "West";
			default:
return "Unknown";
		}
	}
	
	public void forward() {
		this.moveInDirection(1);
	}
	
	public void backward() {
		this.moveInDirection(-1);
	}
	
	//Currently, only 1 (forward) and -1 (backward) allowed
	public void moveInDirection(int numBlocks) {
		if (numBlocks == 1 || numBlocks == -1) {
			int tarX = this.getX();
			int tarY = this.getY();

			switch (this.getDirection()) {
			case Rover.DIRECTION_EAST:
				tarX = tarX + numBlocks;
				break;
			case Rover.DIRECTION_NORTH:
				tarY = tarY + numBlocks;
				break;
			case Rover.DIRECTION_SOUTH:
				tarY = tarY - numBlocks;
				break;
			case Rover.DIRECTION_WEST:
				tarX = tarX - numBlocks;
				break;
			}

			if (this.landscape != null) {
				//Check collision
				if (this.landscape.hasObject(tarX, tarY)) {
					throw new IllegalArgumentException(Rover.ERROR_MOVE_COLLISION);
				}
			}
			
			this.setX(tarX);
			this.setY(tarY);
		} else {
			throw new IllegalArgumentException(Rover.ERROR_MOVE_INVALID_NUM_BLOCKS);
		}
	}
	
	public void rotateClockwise() {
		switch (this.getDirection()) {
		case Rover.DIRECTION_EAST:
			this.setDirection(Rover.DIRECTION_SOUTH);
			return;
		case Rover.DIRECTION_NORTH:
			this.setDirection(Rover.DIRECTION_EAST);
			return;
		case Rover.DIRECTION_SOUTH:
			this.setDirection(Rover.DIRECTION_WEST);
			return;
		case Rover.DIRECTION_WEST:
			this.setDirection(Rover.DIRECTION_NORTH);
			return;
		}
	}
	
	public void rotateAnticlockwise() {
		switch (this.getDirection()) {
		case Rover.DIRECTION_EAST:
			this.setDirection(Rover.DIRECTION_NORTH);
			return;
		case Rover.DIRECTION_NORTH:
			this.setDirection(Rover.DIRECTION_WEST);
			return;
		case Rover.DIRECTION_SOUTH:
			this.setDirection(Rover.DIRECTION_EAST);
			return;
		case Rover.DIRECTION_WEST:
			this.setDirection(Rover.DIRECTION_SOUTH);
			return;
		}
	}
	
	public String toString() {
		return String.format("Rover (%d, %d) [%s]", this.getX(), this.getY(), this.getHumanFriendlyDirection());
	}

}
