package com.chadin.rovercontroller;

import java.util.HashMap;
import java.util.Iterator;

//Represents the Mars landscape that the rover resides in
public class Landscape {

	public static final String ERROR_INIT_ROVER_ID_ALREADY_EXISTS = "Unable to initialize rover, as a rover with specified ID already exists";
	public static final String ERROR_INIT_ROVER_COLLISION = "Unable to initialize rover, as a rover with specified ID already exists";
	
	private HashMap<Integer, Rover> rovers;
	
	public Landscape() {
		this.rovers = new HashMap<Integer, Rover>();
	}
	
	public void initRover(int id, int initX, int initY, char initDirection) {
		//Check collision
		if (this.hasObject(initX, initY)) {
			throw new IllegalArgumentException(Landscape.ERROR_INIT_ROVER_COLLISION);
		}
		
		if (this.rovers.containsKey(id)) {
			throw new IllegalArgumentException(Landscape.ERROR_INIT_ROVER_ID_ALREADY_EXISTS);
		}
		
		Rover newRover = new Rover(this, initX, initY, initDirection);
		this.rovers.put(id, newRover);
	}
	
	public void printStatus() {
		Iterator it = this.rovers.entrySet().iterator();
		System.out.println("---");
		while (it.hasNext()) {
			HashMap.Entry<Integer, Rover> pair = (HashMap.Entry<Integer, Rover>) it.next();
			System.out.printf("[%d] %s\n", pair.getKey(), pair.getValue());
		}
		System.out.println("---");
	}

	public boolean hasObject(int x, int y) {
		Iterator it = this.rovers.entrySet().iterator();
		while (it.hasNext()) {
			HashMap.Entry<Integer, Rover> pair = (HashMap.Entry<Integer, Rover>) it.next();
			if (pair.getValue().getX() == x && 
					pair.getValue().getY() == y) {
				return true;
			}
		}
		return false;
	}
	
	public Rover getRover(int id) {
		if (!this.rovers.containsKey(id)) {
			return null;
		}
		
		return this.rovers.get(id);
	}
}
