package com.chadin.rovercontroller;

import java.util.Scanner;

public class MultiRoverController {
	
	public static final String ERROR_INVALID_COMMAND = "Invalid command in the command sequence";
	public static final String ERROR_ROVER_NOT_FOUND = "Rover not found";
	private static Landscape landscape;
	
	public static void init() {
		landscape = new Landscape();
	}

	public static void main(String[] args) {
		MultiRoverController.init();
		
		//Await commands
		Scanner sc = new Scanner(System.in) ;
		String line = "";
		while(true) {
			System.out.print("Please enter the next command: ");
			 line = sc.nextLine();
			 if (line.equals("exit")) {
				 System.out.println("Goodbye.");
				 break;
			 }
			 try {
				 inputCommand(line);
			 } catch (IllegalArgumentException ex) {
				 System.out.println(ex.getMessage());
			 }
		}
		

		//CLI documentation
		
		//new [initX] [initY] [initDirection]
		//Initialize new rover
		
		//[roverID] [commands]
		//Send command to rover with designated rover ID.
		//Multiple commands can be sent. If a command cannot be done 
		//because another rover is in the way, the rover will not execute 
		//the command, drop the remaining commands, and display a clear error 
		//to the user.
		
		//display
		//Print a map showing all the positions of the rover
		//This is done so that the user has a more intuitive way of knowing where a rover is
	}

	public static Landscape getLandscape() {
		return landscape;
	}

	public static void inputCommand(String line) {
		String[] tokens = line.split(" ");
		
		if (tokens.length >= 1) {
			if (tokens[0].equals("new")) {
				int id = Integer.parseInt(tokens[1]);
				int initX = Integer.parseInt(tokens[2]);
				int initY = Integer.parseInt(tokens[3]);
				char initDirection = tokens[4].charAt(0);
				landscape.initRover(id, initX, initY, initDirection);
			} else if (tokens[0].equals("status")) {
				landscape.printStatus();
			} else {
				int roverID = Integer.parseInt(tokens[0]);
				
				for (int i = 1; i < tokens.length; i++) {
					//Validate each command
					if (!tokens[i].equals("f") &&
							!tokens[i].equals("b") &&
							!tokens[i].equals("l") &&
							!tokens[i].equals("r")) {
						throw new IllegalArgumentException(ERROR_INVALID_COMMAND);
					}
				}

				Rover r = landscape.getRover(roverID);
				if (r == null) {
					throw new IllegalArgumentException(ERROR_ROVER_NOT_FOUND);
				}
				
				for (int i = 1; i < tokens.length; i++) {
					switch (tokens[i]) {
					case "f":
						r.forward();
						break;
					case "b":
						r.backward();
						break;
					case "l":
						r.rotateAnticlockwise();
						break;
					case "r":
						r.rotateClockwise();
						break;
					}
				}
			}
		}
	}

}
