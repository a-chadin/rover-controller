package com.chadin.rovercontroller;

public class RoverController {

	private static Rover rover;

	public static void main(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException("Invalid number of arguments");
		}

		String inits = args[0];
		String commands = args[1];

		String[] initsArr = inits.split(",");
		String[] commandsArr = commands.split(",");

		int x = Integer.parseInt(initsArr[0]);
		int y = Integer.parseInt(initsArr[1]);
		char direction = initsArr[2].charAt(0);

		RoverController.rover = new Rover(null, x, y, direction);

		//System.out.printf("%s\n", RoverController.rover);

		for (int i = 0; i < commandsArr.length; i++) {
			char cmd = commandsArr[i].charAt(0);

			switch (cmd) {
			case 'f':
				RoverController.rover.forward();
				break;
			case 'b':
				RoverController.rover.backward();
				break;
			case 'r':
				RoverController.rover.rotateClockwise();
				break;
			case 'l':
				RoverController.rover.rotateAnticlockwise();
				break;
			default:
				throw new IllegalArgumentException("Invalid command");
			}

			//System.out.printf("%s\n", RoverController.rover);
		}
		
		System.out.printf("Final Coordinates: %d, %d\n", RoverController.rover.getX(), RoverController.rover.getY());
		System.out.printf("Final Direction: %s\n", RoverController.rover.getHumanFriendlyDirection());
	}

	public static Rover getRover() {
		return RoverController.rover;
	}

}
