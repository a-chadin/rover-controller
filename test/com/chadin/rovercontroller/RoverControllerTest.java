package com.chadin.rovercontroller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class RoverControllerTest {

	@Test
	void testMain() {
		String[] args = {"3,4,N", "f,f,r,f,f"};
		RoverController.main(args);

		assertEquals(RoverController.getRover().getX(), 5, "x");
		assertEquals(RoverController.getRover().getY(), 6, "y");
		assertEquals(RoverController.getRover().getDirection(), Rover.DIRECTION_EAST, "direction");
	}

	@Test
	void testMainInvalidNumberOfArgs() {
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			String[] args = {"3,4,N"};
			RoverController.main(args);
	    });
	 
	    String expectedMessage = "Invalid number of arguments";
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	void testMainInvalidCommand() {
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			String[] args = {"3,4,N", "f,x,r,f,f"};
			RoverController.main(args);
	    });
	 
	    String expectedMessage = "Invalid command";
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

}
