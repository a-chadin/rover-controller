package com.chadin.rovercontroller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MultiRoverControllerTest {

	@Test
	void testMain() {
		String[] args = {};
		MultiRoverController.init();

		MultiRoverController.inputCommand("new 1 1 1 N");
		
		MultiRoverController.getLandscape().printStatus();
		
		Rover r = MultiRoverController.getLandscape().getRover(1);
		
		assertNotEquals(r, null, "get rover 1");

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 1, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");

		MultiRoverController.inputCommand("1 f f f f f");
		
		MultiRoverController.getLandscape().printStatus();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 6, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");

		MultiRoverController.inputCommand("1 b");
		
		MultiRoverController.getLandscape().printStatus();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 5, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");

		MultiRoverController.inputCommand("1 r");
		
		MultiRoverController.getLandscape().printStatus();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 5, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");

		MultiRoverController.inputCommand("1 l");
		
		MultiRoverController.getLandscape().printStatus();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 5, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");

		MultiRoverController.inputCommand("status");
	}

	@Test
	void testMainCollisionAbortHalfway() {
		String[] args = {};
		MultiRoverController.init();

		MultiRoverController.inputCommand("new 1 1 1 N");
		
		MultiRoverController.getLandscape().printStatus();
		
		MultiRoverController.inputCommand("new 2 1 3 N");
		
		MultiRoverController.getLandscape().printStatus();
		
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			MultiRoverController.inputCommand("1 f f f f f");
	    });
		
		MultiRoverController.getLandscape().printStatus();
	 
	    String expectedMessage = Rover.ERROR_MOVE_COLLISION;
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
		
		Rover r = MultiRoverController.getLandscape().getRover(1);
		
		assertNotEquals(r, null, "get rover 1");

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 2, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");
	}

	@Test
	void testMainRoverNotFound() {
		String[] args = {};
		MultiRoverController.init();

		MultiRoverController.inputCommand("new 1 1 1 N");
		
		MultiRoverController.getLandscape().printStatus();
		
		MultiRoverController.inputCommand("new 2 1 3 N");
		
		MultiRoverController.getLandscape().printStatus();
		
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			MultiRoverController.inputCommand("3 f f f f f");
	    });
		
		MultiRoverController.getLandscape().printStatus();
	 
	    String expectedMessage = MultiRoverController.ERROR_ROVER_NOT_FOUND;
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	void testMainInvalidCommandInSequence() {
		String[] args = {};
		MultiRoverController.init();

		MultiRoverController.inputCommand("new 1 1 1 N");
		
		MultiRoverController.getLandscape().printStatus();
		
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			MultiRoverController.inputCommand("1 f f f f f x");
	    });
		
		MultiRoverController.getLandscape().printStatus();
	 
	    String expectedMessage = MultiRoverController.ERROR_INVALID_COMMAND;
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

}
