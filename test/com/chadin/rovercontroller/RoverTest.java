package com.chadin.rovercontroller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RoverTest {

	@Test
	void testConstructor() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 2, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");
	}

	@Test
	void testForward() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		r.forward();

		assertEquals(r.getX(), 2, "x");
		assertEquals(r.getY(), 2, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_NORTH);
		
		r.forward();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 3, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_SOUTH);
		
		r.forward();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 1, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_WEST);
		
		r.forward();

		assertEquals(r.getX(), 0, "x");
		assertEquals(r.getY(), 2, "y");
	}

	@Test
	void testBackward() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		r.backward();

		assertEquals(r.getX(), 0, "x");
		assertEquals(r.getY(), 2, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_NORTH);
		
		r.backward();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 1, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_SOUTH);
		
		r.backward();

		assertEquals(r.getX(), 1, "x");
		assertEquals(r.getY(), 3, "y");
		
		r = new Rover(null, 1, 2, Rover.DIRECTION_WEST);
		
		r.backward();

		assertEquals(r.getX(), 2, "x");
		assertEquals(r.getY(), 2, "y");
	}

	@Test
	void testRotateClockwise() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		r.rotateClockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_SOUTH, "direction");
		
		r.rotateClockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_WEST, "direction");
		
		r.rotateClockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");
		
		r.rotateClockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");
	}

	@Test
	void testRotateAnticlockwise() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		r.rotateAnticlockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");
		
		r.rotateAnticlockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_WEST, "direction");
		
		r.rotateAnticlockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_SOUTH, "direction");
		
		r.rotateAnticlockwise();
		
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");
	}

	@Test
	void testSetDirection() {
		Rover r = new Rover(null, 1, 2, Rover.DIRECTION_EAST);
		
		r.setDirection(Rover.DIRECTION_NORTH);
		
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");

	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			r.setDirection('q');
	    });
	 
	    String expectedMessage = "Invalid direction to set";
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

}
