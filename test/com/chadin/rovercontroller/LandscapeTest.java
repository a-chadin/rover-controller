package com.chadin.rovercontroller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class LandscapeTest {

	@Test
	void testAddRover() {
		Landscape l = new Landscape();
		
		l.initRover(1, 2, 3, Rover.DIRECTION_EAST);
		
		l.printStatus();
		
		assertNotEquals(l.getRover(1), null, "get rover 1");

		Rover r = l.getRover(1);
		assertEquals(r.getX(), 2, "x");
		assertEquals(r.getY(), 3, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");
		
		//Error 1
	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			l.initRover(1, 2, 3, Rover.DIRECTION_EAST);
	    });
	 
	    String expectedMessage = Landscape.ERROR_INIT_ROVER_ID_ALREADY_EXISTS;
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	    
	    //Error 2
	     exception = assertThrows(IllegalArgumentException.class, () -> {
			l.initRover(2, 2, 3, Rover.DIRECTION_EAST);
	    });
	 
	     expectedMessage = Landscape.ERROR_INIT_ROVER_COLLISION;
	     actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
		
		l.initRover(2, -1, -2, Rover.DIRECTION_NORTH);
		
		l.printStatus();
		
		assertNotEquals(l.getRover(1), null, "get rover 1");

		 r = l.getRover(1);
		assertEquals(r.getX(), 2, "x");
		assertEquals(r.getY(), 3, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_EAST, "direction");
		
		assertNotEquals(l.getRover(2), null, "get rover 2");

		 r = l.getRover(2);
		assertEquals(r.getX(), -1, "x");
		assertEquals(r.getY(), -2, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");
		
		r.forward();

		l.printStatus();

		 r = l.getRover(2);
		assertEquals(r.getX(), -1, "x");
		assertEquals(r.getY(), -1, "y");
		assertEquals(r.getDirection(), Rover.DIRECTION_NORTH, "direction");
	}

	@Test
	void testCollision() {
		Landscape l = new Landscape();
		
		l.initRover(1, 2, 3, Rover.DIRECTION_EAST);
		l.initRover(2, 4, 3, Rover.DIRECTION_EAST);
		
		l.printStatus();
		
		Rover r = l.getRover(2);
		
		r.rotateClockwise();
		l.printStatus();
		
		r.rotateClockwise();
		l.printStatus();
		
		r.forward();
		l.printStatus();

	    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			r.forward();
	    });

		l.printStatus();
	 
	    String expectedMessage = Rover.ERROR_MOVE_COLLISION;
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
		
	}

}
